#include "lalalaFrame.h"
#include "GroupedListViewSample.h"

using namespace Tizen::App;
using namespace Tizen::Ui;

lalalaFrame::lalalaFrame(void)
{
}

lalalaFrame::~lalalaFrame(void)
{
}

result
lalalaFrame::OnInitializing(void)
{
	result r = E_SUCCESS;

	GroupedListViewSample* groupList = new GroupedListViewSample();
	groupList->Initialize();
	AddControl(groupList);
	SetCurrentForm(groupList);
	groupList->Invalidate(true);

	SetPropagatedKeyEventListener(this);
	return r;
}

result
lalalaFrame::OnTerminating(void)
{
	result r = E_SUCCESS;

	// TODO: Add your frame termination code here.
	return r;
}

bool
lalalaFrame::OnKeyReleased(Tizen::Ui::Control& source, const Tizen::Ui::KeyEventInfo& keyEventInfo)
{
	KeyCode keyCode = keyEventInfo.GetKeyCode();

	if (keyCode == KEY_BACK)
	{
		UiApp* pApp = UiApp::GetInstance();
		AppAssert(pApp);
		pApp->Terminate();
	}

	return false;
}
