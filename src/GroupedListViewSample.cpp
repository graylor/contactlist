//Sample code for GroupedListViewSample.cpp
#include <FApp.h>
#include <FGraphics.h>

#include "GroupedListViewSample.h"

using namespace Tizen::App;
using namespace Tizen::Base;
using namespace Tizen::Graphics;
using namespace Tizen::Ui::Controls;

class CustomGroupedListElement
    : public ICustomElement
{
public:

    bool
    OnDraw(Canvas& canvas, const Rectangle& rect, ListItemDrawingStatus itemStatus)
    {
        //canvas.SetBackgroundColor(Color::GetColor(COLOR_ID_BLACK));

        Tizen::Graphics::Font font;
        /*font.Construct(FONT_STYLE_PLAIN, 15);
        canvas.SetFont(font);
        canvas.SetLineWidth(3);
        canvas.SetForegroundColor(Color::GetColor(COLOR_ID_GREEN));

        if (canvas.DrawRectangle(rect) != E_SUCCESS)
        {
            return false;
        }

        if (canvas.DrawText(Point(rect.x+10, rect.y+15), L"Custom") != E_SUCCESS)
        {
            return false;
        }

        canvas.SetBackgroundColor(Color::GetColor(COLOR_ID_WHITE));
        Rectangle circle = rect;
        circle.x -= 20;
        circle.y -= 20;
        canvas.DrawEllipse(circle);*/

        return true;
    }
};

bool
GroupedListViewSample::Initialize(void)
{
    Construct(FORM_STYLE_NORMAL);
    return true;
}

result
GroupedListViewSample::OnInitializing(void)
{
    result r = E_SUCCESS;

    // Creates an instance of IconListView
    __pGroupedListView = new GroupedListView();
    __pGroupedListView->Construct(Rectangle(0, 0, GetClientAreaBounds().width, GetClientAreaBounds().height), GROUPED_LIST_VIEW_STYLE_INDEXED, true, true);
    __pGroupedListView->SetItemProvider(*this);
    __pGroupedListView->AddGroupedListViewItemEventListener(*this);
    __pGroupedListView->SetBackgroundColor(Color::GetColor(COLOR_ID_BLACK));
    __pGroupedListView->SetChromaKeyColor(Color::GetColor(COLOR_ID_BLUE));
    __pGroupedListView->SetItemDividerColor(Color::GetColor(COLOR_ID_GREY));
    __pGroupedListView->SetSectionColor(Color::GetColor(COLOR_ID_BLUE));

    // Adds the icon list view to the form
    AddControl(__pGroupedListView);

    // Creates an instance of ListContextItem
    __pItemContext = new ListContextItem();
    __pItemContext->Construct();
    __pItemContext->AddElement(ID_CONTEXT_ITEM_1, "Сообщение");
    __pItemContext->AddElement(ID_CONTEXT_ITEM_2, "Звонок");

    // Gets instances of Bitmap
    AppResource* pAppResource = Application::GetInstance()->GetAppResource();
    __pImg = pAppResource->GetBitmapN(L"tizen.png");

    // Creates an instance of CustomGroupedListElement
    __pCustomGroupedListElement = new CustomGroupedListElement();

    return r;
}

result
GroupedListViewSample::OnTerminating(void)
{
    result r = E_SUCCESS;

    // Deallocates bitmaps
    delete __pImg;

    // Deallocates the item context
    delete __pItemContext;

    return r;
}

void
GroupedListViewSample::OnGroupedListViewItemStateChanged(GroupedListView &listView, int groupIndex, int itemIndex, int elementId, ListItemStatus state)
{
    switch (elementId)
    {
    case ID_FORMAT_STRING:
        {
            // ....
        }
        break;
    case ID_FORMAT_BITMAP:
        {
            // ....
        }
        break;
    default:
        break;
    }
}

void
GroupedListViewSample::OnGroupedListViewContextItemStateChanged(GroupedListView &listView, int groupIndex, int itemIndex, int elementId, ListContextItemStatus state)
{
    switch (elementId)
    {
    case ID_CONTEXT_ITEM_1:
        {
            // ....
        }
        break;
    case ID_CONTEXT_ITEM_2:
        {
            // ....
        }
        break;
    default:
        break;
    }
}

// IGroupedListViewItemEventListener
void
GroupedListViewSample::OnGroupedListViewItemSwept(GroupedListView &listView, int groupIndex,  int itemIndex, SweepDirection direction)
{
    // ....
}

int
GroupedListViewSample::GetGroupCount(void)
{
    return 3;
}

int
GroupedListViewSample::GetItemCount(int groupIndex)
{
    int itemCount = 0;
    switch (groupIndex)
    {
    case 0:
        {
            itemCount = 7;
        }
        break;
    case 1:
        {
            itemCount = 5;
        }
        break;
    case 2:
        {
            itemCount = 3;
        }
        break;
    default:
        break;
    }

    return itemCount;
}

// IGroupedListViewItemProvider
GroupItem*
GroupedListViewSample::CreateGroupItem(int groupIndex, int itemWidth)
{
    String text("Group ");
    text.Append(groupIndex+1);

    GroupItem* pItem = new GroupItem();
    pItem->Construct(Dimension(itemWidth, 40));
    pItem->SetElement(text, null);

    return pItem;
}

ListItemBase*
GroupedListViewSample::CreateItem(int groupIndex, int itemIndex, int itemWidth)
{
    // Creates an instance of CustomItem
    CustomItem* pItem = new CustomItem();
    ListAnnexStyle style = LIST_ANNEX_STYLE_NORMAL;
    //ListAnnexStyle style = LIST_ANNEX_STYLE_ONOFF_SLIDING;
    pItem->Construct(Dimension(itemWidth, 100), style);

    switch (itemIndex % 4)
    {
    case 0:
        {
            style = LIST_ANNEX_STYLE_NORMAL;
            pItem->AddElement(Rectangle(10, 20, 60, 60), ID_FORMAT_BITMAP, *__pImg, null, null);
            //pItem->AddElement(Rectangle(150, 25, 150, 50), ID_FORMAT_STRING, L"Home", true);
            pItem->AddElement(Rectangle(150, 25, 150, 50), ID_FORMAT_STRING, L"Call", 40, Color::GetColor(COLOR_ID_WHITE), Color::GetColor(COLOR_ID_WHITE), Color::GetColor(COLOR_ID_WHITE) );
        }
        break;
    case 1:
        {
            style = LIST_ANNEX_STYLE_MARK;
            pItem->AddElement(Rectangle(10, 20, 60, 60), ID_FORMAT_BITMAP, *__pImg, null, null);
            //pItem->AddElement(Rectangle(150, 25, 150, 50), ID_FORMAT_STRING, L"Msg", true);
            pItem->AddElement(Rectangle(150, 25, 150, 50), ID_FORMAT_STRING, L"Call", 40, Color::GetColor(COLOR_ID_WHITE), Color::GetColor(COLOR_ID_WHITE), Color::GetColor(COLOR_ID_WHITE) );
        }
        break;
    case 2:
        {
            style = LIST_ANNEX_STYLE_ONOFF_SLIDING;
            pItem->AddElement(Rectangle(10, 20, 60, 60), ID_FORMAT_BITMAP, *__pImg, null, null);
            //pItem->AddElement(Rectangle(150, 25, 150, 50), ID_FORMAT_STRING, L"Alarm", true);
            pItem->AddElement(Rectangle(150, 25, 150, 50), ID_FORMAT_STRING, L"Call", 40, Color::GetColor(COLOR_ID_WHITE), Color::GetColor(COLOR_ID_WHITE), Color::GetColor(COLOR_ID_WHITE) );
        }
        break;
    case 3:
        {
            style = LIST_ANNEX_STYLE_DETAILED;
            pItem->AddElement(Rectangle(10, 20, 60, 60), ID_FORMAT_BITMAP, *__pImg, null, null);
            //pItem->AddElement(Rectangle(150, 25, 150, 50), ID_FORMAT_STRING, L"Call", true);
            pItem->AddElement(Rectangle(150, 25, 150, 50), ID_FORMAT_STRING, L"Call", 40, Color::GetColor(COLOR_ID_WHITE), Color::GetColor(COLOR_ID_WHITE), Color::GetColor(COLOR_ID_WHITE) );
        }
        break;
    default:
        break;
    }
    pItem->AddElement(Rectangle(360, 10, 180, 80), ID_FORMAT_CUSTOM, *(static_cast<ICustomElement *>(__pCustomGroupedListElement)));
    pItem->SetContextItem(__pItemContext);

    pItem->SetDescriptionText("lalala");
    pItem->SetDescriptionTextColor(Color::GetColor(COLOR_ID_WHITE));

    return pItem;
}

bool
GroupedListViewSample::DeleteItem(int groupIndex, int itemIndex, ListItemBase* pItem, int itemWidth)
{
    delete pItem;
    pItem = null;
    return true;
}

bool
GroupedListViewSample::DeleteGroupItem(int groupIndex, GroupItem* pItem, int itemWidth)
{
    delete pItem;
    pItem = null;
    return true;
}
