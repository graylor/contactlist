#include <new>
#include "lalalaApp.h"
#include "lalalaFrame.h"

using namespace Tizen::App;
using namespace Tizen::System;

lalalaApp::lalalaApp(void)
{
}

lalalaApp::~lalalaApp(void)
{
}

UiApp*
lalalaApp::CreateInstance(void)
{
	// Create the application instance through the constructor.
	return new (std::nothrow) lalalaApp();
}

bool
lalalaApp::OnAppInitializing(AppRegistry& appRegistry)
{
	// TODO: Initialize application-specific data.
	// The permanent data and context of the application can be obtained from the application registry (appRegistry).
	//
	// If this method is successful, return true; otherwise, return false and the application is terminated.


	// Uncomment the following statement to listen to the screen on and off events:
	// PowerManager::SetScreenEventListener(*this);


	// Create the application frame.
	lalalaFrame* plalalaFrame = new (std::nothrow) lalalaFrame;
	TryReturn(plalalaFrame != null, false, "The memory is insufficient.");
	plalalaFrame->Construct();
	plalalaFrame->SetName(L"lalala");
	AddFrame(*plalalaFrame);

	// TODO: Add your application initialization code here.
	return true;
}

bool
lalalaApp::OnAppInitialized(void)
{
	return true;
}

bool
lalalaApp::OnAppWillTerminate(void)
{
	// TODO: Deallocate or release resources in devices that have the END key.
	return true;
}

bool
lalalaApp::OnAppTerminating(AppRegistry& appRegistry, bool forcedTermination)
{
	// TODO: Deallocate all resources allocated by the application.
	// The permanent data and context of the application can be saved through the application registry (appRegistry).
	return true;
}

void
lalalaApp::OnForeground(void)
{
	// TODO: Start or resume drawing when the application is moved to the foreground.
}

void
lalalaApp::OnBackground(void)
{
	// TODO: Stop drawing when the application is moved to the background to save the CPU and battery consumption.
}

void
lalalaApp::OnLowMemory(void)
{
	// TODO: Free unnecessary resources or close the application.
}

void
lalalaApp::OnBatteryLevelChanged(BatteryLevel batteryLevel)
{
	// TODO: Handle all battery level changes here.
	// Stop using multimedia features (such as camera and mp3 playback) if the battery level is CRITICAL.
}

void
lalalaApp::OnScreenOn(void)
{
	// TODO: Retrieve the released resources or resume the operations that were paused or stopped in the OnScreenOff() event handler.
}

void
lalalaApp::OnScreenOff(void)
{
	// TODO: Release resources (such as 3D, media, and sensors) to allow the device to enter the sleep mode 
	// to save the battery (unless you have a good reason to do otherwise).
	// Only perform quick operations in this event handler. Any lengthy operations can be risky; 
	// for example, invoking a long asynchronous method within this event handler can cause problems
	// because the device can enter the sleep mode before the callback is invoked.

}
