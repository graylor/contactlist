/*
 * GroupedListViewSample.h
 *
 *  Created on: Nov 12, 2013
 *      Author: Sergey
 */

#ifndef GROUPEDLISTVIEWSAMPLE_H_
#define GROUPEDLISTVIEWSAMPLE_H_

#include <FUi.h>

class CustomGroupedListElement;

class GroupedListViewSample
    : public Tizen::Ui::Controls::Form
    , public Tizen::Ui::Controls::IGroupedListViewItemEventListener
    , public Tizen::Ui::Controls::IGroupedListViewItemProvider
{
public:
    GroupedListViewSample(void)
    : __pGroupedListView(null)
    , __pCustomGroupedListElement(null){}

    bool Initialize(void);
    virtual result OnInitializing(void);
    virtual result OnTerminating(void);

    // IGroupedListViewItemEventListener
    virtual void OnGroupedListViewContextItemStateChanged(Tizen::Ui::Controls::GroupedListView &listView, int groupIndex, int itemIndex, int elementId, Tizen::Ui::Controls::ListContextItemStatus state);
    virtual void OnGroupedListViewItemStateChanged(Tizen::Ui::Controls::GroupedListView &listView, int groupIndex, int itemIndex, int elementId, Tizen::Ui::Controls::ListItemStatus state);
    virtual void OnGroupedListViewItemSwept(Tizen::Ui::Controls::GroupedListView &listView, int groupIndex,  int itemIndex, Tizen::Ui::Controls::SweepDirection direction);

    // IGroupedListViewItemProvider
    virtual int GetGroupCount(void);
    virtual int GetItemCount(int groupIndex);
    virtual Tizen::Ui::Controls::ListItemBase* CreateItem(int groupIndex, int itemIndex, int itemWidth);
    virtual Tizen::Ui::Controls::GroupItem* CreateGroupItem(int groupIndex, int itemWidth);
    virtual bool DeleteItem(int groupIndex, int itemIndex, Tizen::Ui::Controls::ListItemBase* pItem, int itemWidth);
    virtual bool DeleteGroupItem(int groupIndex, Tizen::Ui::Controls::GroupItem* pItem, int itemWidth);

private:
    static const int ID_FORMAT_STRING = 100;
    static const int ID_FORMAT_BITMAP = 101;
    static const int ID_FORMAT_CUSTOM = 102;
    static const int ID_CONTEXT_ITEM_1 = 103;
    static const int ID_CONTEXT_ITEM_2 = 104;

    Tizen::Graphics::Bitmap* __pImg;

    Tizen::Ui::Controls::GroupedListView* __pGroupedListView;
    Tizen::Ui::Controls::ListContextItem* __pItemContext;
    CustomGroupedListElement* __pCustomGroupedListElement;
};


#endif /* GROUPEDLISTVIEWSAMPLE_H_ */
